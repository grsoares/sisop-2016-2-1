#include <ucontext.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../include/support.h"
#include "../include/cdata.h"
#include "../include/cthread.h"
#include "../include/scheduler.h"

extern PFILA2 enabled_queue; //queue containing all the TCBs of enabled threads
extern PFILA2 blocked_queue; //queue containing all the TCBs of threads blocked by a csem_t by calling cwait()
extern PFILA2 joining_queue; //queue containing the joining_TCB structs of all the threads blocked by calling cjoin()

extern TCB_t *executing_thread; //pointer to the TCB currently in execution
extern ucontext_t exit_context;  //context that is launched at the end of each created thread

extern int current_tid;

// seatch_tid: Função auxiliar para procurar um ID fornecido dentro de uma fila de TCBs do tipo PFILA2
// 			   Utilizada para verificação em cjoin
// 			   - retorno: valor 0 caso exista uma thread com o ID fornecido na lista indicada e 1 caso contrário
int search_tid (PFILA2 list, int tid) {
	TCB_t *temp = NULL;
	FirstFila2(list);

	while(GetAtIteratorFila2(list) != NULL) {
		temp = (TCB_t*) GetAtIteratorFila2(list);

		if(temp->tid == tid)
			return 0;

		NextFila2(list);
	}

	return 1;
}

// search_tcb_tid_join: Função auxiliar para procurar um ID fornecido dentre os TCBs da fila joining_queue
// 			  			Utilizada para verificação específica da fila joining_queue em cjoin
// 			 			- retorno: valor 0 caso exista uma thread com o ID fornecido na joining_queue e 1 caso contrário
int search_tcb_tid_join (PFILA2 list, int tid) {
	jTCB_t *jTemp = NULL;
	FirstFila2(list);

	while(GetAtIteratorFila2(list) != NULL) {
		jTemp = (jTCB_t*) GetAtIteratorFila2(list);

		if(jTemp->tcb->tid == tid)
			return 0;

		NextFila2(list);
	}

	return 1;
}

// search_tid_join: Função auxiliar para procurar um ID fornecido dentre os IDs esperados da fila joining_queue
// 			  		Utilizada para verificação específica da fila joining_queue em cjoin
// 			 		- retorno: valor 0 caso exista uma thread com o ID fornecido na joining_queue e 1 caso contrário
int search_tid_join (PFILA2 list, int tid) {
	jTCB_t *jTemp = NULL;
	FirstFila2(list);

	while(GetAtIteratorFila2(list) != NULL) {
		jTemp = (jTCB_t*) GetAtIteratorFila2(list);

		if(jTemp->tid == tid)
			return 0;

		NextFila2(list);
	}

	return 1;
}

// ccreate: Inicialização e criação da thread, colocando-a no estado de Apta inserindo-a na fila de Aptos
//          - retorno: valor do identificador da thread (tid) criada no caso de sucesso ou -1 caso contrário
int ccreate (void* (*start)(void*), void *arg) {
	if(_init_() != 0) {
		// Erro de inicialização
		fprintf(stderr, "Falha de inicialização em ccreate.\n");
      	return -1;
  	}

	TCB_t *new_t = malloc(sizeof(TCB_t));

	if(new_t != NULL){
		int tid = current_tid++;
		new_t->tid = tid;
		new_t->state = 1; // A thread é colocada diretamente no estado de Apta por conveniência
		new_t->ticket = (int) Random2() >> 8;

		getcontext(&new_t->context);
		new_t->context.uc_link = &exit_context;
		new_t->context.uc_stack.ss_sp = malloc(SIGSTKSZ);
		new_t->context.uc_stack.ss_size = SIGSTKSZ;
		makecontext(&(new_t->context), (void (*)())start, 1, arg);

		// Insere a nova thread na fila de Aptos
		if((AppendFila2(enabled_queue, new_t)) != 0)
			fprintf(stderr, "Erro na inserção da nova thread na fila de Aptos.\n");

		// Retorna o id da thread no caso de sucesso
		return tid;
	}

	// Caso ocorra algum erro, retorna um valor negativo
  	return -1;
}

// cyield: Liberação voluntária do uso de CPU pela thread, colocando-a de volta no estado de Apto
//         - retorno: 0 no caso de sucesso ou -1 caso contrário
int cyield(void) {
    if(_init_() != 0) {
		// Erro de inicialização
		fprintf(stderr, "Falha de inicialização em cyield.\n");
        return -1;
    }

	TCB_t *temp = executing_thread;
	temp->state = 1; // Retorna o estado da thread para Apta

	swap_context();
	// Retorna o valor 0 no caso de sucesso
    return 0;
}

// cjoin: Recebendo o identificador de outra thread (aguardada), esta função coloca em espera (bloqueada) a thread que estiver executando-a até que a thread cujo id foi fornecido tenha terminado sua execução
//        - retorno: 0 no caso de sucesso ou -1 caso contrário
int cjoin(int tid) {
	if(_init_() != 0) {
		// Erro de inicialização
		fprintf(stderr, "Falha de inicialização em cjoin.\n");
        return -1;
    }

	// Se o ID fornecido for maior que o do último criado no processo, retorna erro
	if(tid >= current_tid) {
		fprintf(stderr, "ID fornecido maior que todos já criados no sistema.\n");
    	return -1;
	}

	FirstFila2(enabled_queue);
	FirstFila2(joining_queue);
	FirstFila2(blocked_queue);

	// Retorna um erro caso o id já esteja sendo esperado por outro id
	if(!(search_tid_join(joining_queue,tid))) {
		fprintf(stderr, "ID fornecido já está sendo esperado por outra thread no sistema.\n");
    	return -1;
	}
	// Apenas continua a execução normal da thread caso o tid fornecido não seja de nenhuma thread presente nas filas de Aptos ou Bloqueados ou Joining
	if((search_tid(enabled_queue,tid)) && (search_tcb_tid_join(joining_queue,tid)) && (search_tid(blocked_queue,tid))){
        return 0;
  	}

	executing_thread->state = 3;

	jTCB_t *temp = (jTCB_t*)malloc(sizeof(jTCB_t));
	temp->tcb = executing_thread;
	temp->tid = tid;

	if((AppendFila2(joining_queue, temp)) != 0) {
		fprintf(stderr, "Erro inserindo thread na fila de Bloqueados por cjoin.\n");
	}

	swap_context();

	return 0;
}

int cidentify(char *name, int size)
{
	if(_init_() != 0) {
		// Erro de inicialização
		fprintf(stderr, "Falha de inicialização em cidentify.\n");
      	return -1;
  	}
  	strncpy(name, "Alexandre - 193093\nGabriel Restori Soares - 217436\nHermes Tessaro de Mello Affonso - 218317", (size_t)size);
    return 0;
}
