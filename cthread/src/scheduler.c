#include <ucontext.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "../include/support.h"
#include "../include/scheduler.h"
#include "../include/cdata.h"
#include "../include/cthread.h"

PFILA2 enabled_queue; //queue containing all the TCBs of enabled threads
PFILA2 blocked_queue; //queue containing all the TCBs of threads blocked by a csem_t by calling cwait()
PFILA2 joining_queue; //queue containing the joining_TCB structs of all the threads blocked by calling cjoin()

TCB_t *executing_thread; //pointer to the TCB currently in execution
ucontext_t exit_context; //context that is launched at the end of each created thread

int initialized = 0;
int current_tid = 0;

void _exit_ (void) {
    //Enables the threads waiting for the finished thread
    if(FirstFila2(joining_queue) == 0) {
        jTCB_t *current_joining_tcb = (jTCB_t*) GetAtIteratorFila2(joining_queue);
        do {
            if(current_joining_tcb->tid == executing_thread->tid) {
                //if a thread is waiting for this one, moves to the enabled queue and frees the structure memory space
                TCB_t *enabled_tcb = current_joining_tcb->tcb;
                enabled_tcb->state = 1;
                AppendFila2(enabled_queue, enabled_tcb);
                remove_from_queue(joining_queue, current_joining_tcb);
                free(current_joining_tcb);
            }
            NextFila2(joining_queue);
        } while ((current_joining_tcb = (jTCB_t*) GetAtIteratorFila2(joining_queue)) != NULL);
    }

    executing_thread->state = 4;
    swap_context();
    return;
}

int initialize_queues () {
    enabled_queue = (PFILA2)malloc(sizeof(PFILA2));
    blocked_queue = (PFILA2)malloc(sizeof(PFILA2));
    joining_queue = (PFILA2)malloc(sizeof(PFILA2));

    if(CreateFila2(enabled_queue) != 0) {
        return -1;
    }
    if(CreateFila2(blocked_queue) != 0) {
        return -1;
    }
    if(CreateFila2(joining_queue) != 0) {
        return -1;
    }

    return 0;
}

int initialize_exit_context () {
    if(getcontext(&exit_context) != 0) {
        return -1;
    }

    exit_context.uc_stack.ss_sp = malloc(SIGSTKSZ);
    exit_context.uc_stack.ss_size = SIGSTKSZ;

    makecontext(&exit_context, _exit_, 0);

    return 0;
}


int _init_ () {
    if(initialized != 0) {
        return 0;
    }
    initialized = 1;

    if(initialize_queues()  != 0) {
        return -1;
    }
    if(initialize_exit_context() != 0) {
        return -1;
    }

    //creates main context
    executing_thread = (TCB_t*) malloc(sizeof(TCB_t));
    executing_thread->tid = current_tid;
    current_tid++;

    executing_thread->state = 2;
    executing_thread->ticket = (int) Random2() >> 8;
    executing_thread->context.uc_stack.ss_sp = malloc(SIGSTKSZ);
    executing_thread->context.uc_stack.ss_size = SIGSTKSZ;
    if(getcontext(&(executing_thread->context)) != 0) {
        return -1;
    }

    return 0;
}


/* Chooses according to the lottery policy defined in the specification, the next
* thread to be executed. Returns NULL if an error occurred or if there's no thread
* to be selected.*/
TCB_t* choose_next_thread () {
    if(FirstFila2(enabled_queue) != 0) {
        return NULL;
    }
    int smaller_difference;
    int lottery_ticket = Random2() >> 8;

    TCB_t* chosen_thread = NULL;
    chosen_thread = GetAtIteratorFila2(enabled_queue);
    smaller_difference = abs(lottery_ticket - chosen_thread->ticket);

    NextFila2(enabled_queue);
    TCB_t* possible_thread;
    //itera em todas os tcbs na fila de aptos procurando o com ticket mais proximo do ticket sorteado,
    //em caso de igualdade escolhe o menor tid
    while((possible_thread = GetAtIteratorFila2(enabled_queue)) != NULL) {
        int possible_difference = abs(lottery_ticket - possible_thread->ticket);

        if(possible_difference < smaller_difference) {
            smaller_difference = possible_difference;
            chosen_thread = possible_thread;
        } else if(possible_difference == smaller_difference) {
            if(possible_thread->tid < chosen_thread->tid) {
                chosen_thread = possible_thread;
            }
        }
        NextFila2(enabled_queue);
    }

    return chosen_thread;
}

/* Deletes from the queue the element passed as parameter
*  Nothing happens if the element is not in the queue
*  If the same element is more than once in the queue, it will remove all of them
*/
void remove_from_queue(PFILA2 queue, void* element) {
    if(FirstFila2(queue) != 0) {
        return;
    }

    do {
        void* current_element = GetAtIteratorFila2(queue);
        if(current_element == element) {
            DeleteAtIteratorFila2(queue);
        }
    } while (NextFila2(queue) == 0);

    FirstFila2(queue);
    return;
}

int swap_context () {
    TCB_t *next_thread = choose_next_thread();
    if(next_thread == NULL) {
        if(executing_thread->state == 1) {
            //if there's no next thread able to be selected, then the current thread continue its execution
            executing_thread->state = 2;
            return 0;
        } else {
            //if this thread is not in the enabled state, it's either an error or a deadlock
            return -1;
        }
    }
    next_thread->state = 2;
    remove_from_queue(enabled_queue, next_thread);
    ucontext_t *next_context = &(next_thread->context);

    if(executing_thread->state == 1) { //it's the case of a cyield
        AppendFila2(enabled_queue, executing_thread);
    } else if(executing_thread->state == 4) {//if the current thread has ended
        free(executing_thread);

        executing_thread = next_thread;
        setcontext(next_context);
    }
    ucontext_t *previous_context = &(executing_thread->context);
    executing_thread = next_thread;

    swapcontext(previous_context, next_context);

    return 0;
}
