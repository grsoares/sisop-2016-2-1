#include <ucontext.h>
#include <stdlib.h>
#include "../include/support.h"
#include "../include/cdata.h"
#include "../include/cthread.h"
#include "../include/scheduler.h"

extern PFILA2 enabled_queue; //queue containing all the TCBs of enabled threads
extern PFILA2 blocked_queue; //queue containing all the TCBs of threads blocked by a csem_t by calling cwait()
extern TCB_t *executing_thread; //pointer to the TCB currently in execution

/* Inicializa um sem�foro com count e fila de bloqueados*/
int csem_init(csem_t *sem, int count) {
    if(_init_() != 0) {
        return -1;
    }

    sem->count = count;
    sem->fila = (PFILA2)malloc(sizeof(PFILA2));

    if(CreateFila2(sem->fila)!= 0 )
    {
        return -1;
    }
    return 0;
}

/*Checa se a thread deve ser colocada no estado bloqueado ou n�o. Se sim, altera seu estado, a coloca na fila de bloqueados e chama swap context.
  Se n�o, apenas decrementa o count do sem�foro*/
int cwait(csem_t *sem) {
    if(_init_() != 0) {
        return -1;
    }

    sem->count -= 1;
    if(sem->count < 0) {
        executing_thread->state = 3;
        AppendFila2(sem->fila, executing_thread);
        AppendFila2(blocked_queue, executing_thread);
        swap_context();
    }
    return 0;
}

/*Libera recurso para thread: incrementa o contador do sem�foro, retira a thread bloqueada da lista de bloqueados e da lista do sem�foro
  e a coloca na lista de aptos, al�m de mudar seu estado para apto.*/
int csignal(csem_t *sem) {
    if(_init_() != 0) {
        return -1;
    }
    FirstFila2(sem->fila);
    TCB_t *tcb = (TCB_t*) GetAtIteratorFila2(sem->fila);

    if(tcb != NULL) {
        tcb->state = 1;

        AppendFila2(enabled_queue, tcb);

        remove_from_queue(blocked_queue, tcb);
        remove_from_queue(sem->fila, tcb);
        /*
        while(GetAtIteratorFila2(blocked_queue)!= tcb) {
            NextFila2(blocked_queue);
        }
        DeleteAtIteratorFila2(blocked_queue);
        DeleteAtIteratorFila2(sem->fila);*/
    }

    sem->count += 1;
    return 0;
}
