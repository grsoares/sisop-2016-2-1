#include <stdio.h>
#include <stdlib.h>
#include "../include/support.h"
#include "../include/cthread.h"

csem_t *my_semaphor;
int i;
void* function1 (void *arg) {

	cwait(my_semaphor);
	printf("Thread número %i, entrou na secao critica.\n", *((int*)arg));
	for(i = 0; i < 10; i++) {
		printf("O valor atual da variavel compartilhada e %i, na thread %i. Executando yield...\n", i, *((int*)arg));
		cyield();
	}
	csignal(my_semaphor);
	printf("Thread número %i, saiu da secao critica.\n", *((int*)arg));
}

int main() {
	my_semaphor = (csem_t*)malloc(sizeof(csem_t));
	csem_init(my_semaphor, 1);
	printf("Inicializou semaforo\n");

	int tids[10];
	int j;

	for(j = 0; j < 10; j++) {
        int *num_thread = (int*)malloc(sizeof(int));
        *num_thread = j+1;
        tids[j] = ccreate(function1, num_thread);
        printf("Criou %i\n", j+1);
    }
	cyield();
	for(j = 0; j < 10; j++) {
        cjoin(tids[j]);
        printf("Joined %i\n", j+1);
    }

	return 0;
}
