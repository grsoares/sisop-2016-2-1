#include <stdio.h>
#include <stdlib.h>
#include "../include/support.h"
#include "../include/cthread.h"

void* function1 (void *arg) {
    if(arg != NULL) {
        printf("Thread esperando a thread de tid %i executa isso primeiro.\n", *((int*)arg));
        cyield();
        printf("Thread esperando a thread de tid %i executa isso depois.\n", *((int*)arg));
        cjoin(*((int*)arg));
        printf("Thread esperando a thread de tid %i terminou.\n", *((int*)arg));
    } else {
        printf("Primeira thread executa isso primeiro.\n");
        cyield();
        printf("Primeira thread terminou.\n");
    }
    return NULL;
}

//The threads should finish in order because each one joins the previous created thread except the first
int main() {
    int i;
    int last_tid;
    last_tid = ccreate(function1, NULL);
    printf("Criou tid %i\n", last_tid);
    for(i = 1; i < 10; i++) {
        int *thread_to_wait = (int*)malloc(sizeof(int));
        *thread_to_wait = last_tid;
        last_tid = ccreate(function1, thread_to_wait);
        printf("Criou tid %i\n", last_tid);
    }

    printf("Main tentou join tid %i\n", last_tid);
    cjoin(last_tid);
    printf("Main joined tid %i\n", last_tid);

    return 0;
}
