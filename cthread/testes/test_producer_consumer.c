#include <stdio.h>
#include <stdlib.h>
#include "../include/support.h"
#include "../include/cthread.h"

#define THREADS 5
#define BUFFER_SIZE 3
#define PRODUCES 5

int buffer[BUFFER_SIZE];
int id;

csem_t *buffer_empty_sem;
csem_t *buffer_full_sem;

csem_t *buffer_lock;

void* produce (void *arg) {
    int thread_number = *(int*)arg;
    int produced_value, i;

    for(i = 0; i < PRODUCES; i++) {
        produced_value = (i * 917) % 13;
        cwait(buffer_full_sem); //producer can't produce if the buffer is full
        cwait(buffer_lock);
        buffer[id++] = produced_value;
        printf("Thread produtora numero %i, inseriu valor %i no buffer. %i\n", thread_number, produced_value, id);
        cyield();//to reaally test it
        csignal(buffer_lock);
        csignal(buffer_empty_sem);
        cyield();
    }
    return;
}

void* consume (void *arg) {
    int thread_number = *(int*)arg;
    int consumed_value, i;

    for(i = 0; i < PRODUCES; i++) {
        cwait(buffer_empty_sem); //consumer can't consume if the buffer is empty
        cwait(buffer_lock);
        consumed_value = buffer[--id];
        printf("Thread consumidora numero %i, consumiu valor %i do buffer. %i\n", thread_number, consumed_value, id);
        cyield();//to reaally test it
        csignal(buffer_lock);
        csignal(buffer_full_sem);
        cyield();
    }
    return;
}

int main() {
    int tids[THREADS*2];
    buffer_empty_sem = (csem_t*)malloc(sizeof(csem_t));
    buffer_full_sem = (csem_t*)malloc(sizeof(csem_t));
    buffer_lock = (csem_t*)malloc(sizeof(csem_t));

    csem_init(buffer_empty_sem, 0);
    csem_init(buffer_full_sem, BUFFER_SIZE);
    csem_init(buffer_lock, 1);

    id = 0;
    int i;
    for(i = 0; i < THREADS*2; i+=2) {
        int *producer_number = (int*)malloc(sizeof(int));
        *producer_number = i;
        tids[i] = ccreate(produce, producer_number);
        int *consumer_number = (int*)malloc(sizeof(int));
        *consumer_number = i+1;
        tids[i+1] = ccreate(consume, consumer_number);
    }

    for(i = 0; i < THREADS*2; i++) {
        cjoin(tids[i]);
        printf("Main joined %i\n", tids[i]);
    }

    return 0;
}
