#include <stdio.h>
#include <stdlib.h>
#include "../include/support.h"
#include "../include/cthread.h"

void* function1 (void *arg) {
    printf("Thread numero %i executa isso primeiro.\n", *((int*)arg));
    cyield();
    printf("Thread numero %i executa isso depois.\n", *((int*)arg));
    return NULL;
}

int main() {
    int tids[10];

    int i;
    for(i = 0; i < 10; i++) {
        int *num_thread = (int*)malloc(sizeof(int));
        *num_thread = i+1;
        tids[i] = ccreate(function1, num_thread);
        printf("Criou %i\n", i+1);
    }

    for(i = 0; i < 10; i++) {
        cjoin(tids[i]);
        printf("Joined %i\n", i+1);
    }

    return 0;
}
