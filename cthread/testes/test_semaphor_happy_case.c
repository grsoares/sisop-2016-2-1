#include <stdio.h>
#include <stdlib.h>
#include "../include/support.h"
#include "../include/cthread.h"

csem_t *my_semaphor;

int main() {
	my_semaphor = (csem_t*)malloc(sizeof(csem_t));
	csem_init(my_semaphor, 1);
	printf("Inicializou semaforo\n");
	cwait(my_semaphor);
	printf("Pegou recurso do semaforo\n");
	csignal(my_semaphor);
	printf("Liberou recurso do semaforo\n");
	return 0;
}
