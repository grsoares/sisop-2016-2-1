#include <stdio.h>
#include <stdlib.h>
#include "../include/support.h"
#include "../include/cthread.h"

void* function1 (void *arg) {
  printf("Primeira thread criada executa isso.\n");
  return NULL;
}

void* function2 (void *arg) {
  printf("Segunda thread criada executa isso.\n");
  return NULL;
}


int main() {
    int tid1 = ccreate(function1, NULL);
    printf("Criou 1\n");
    int tid2 = ccreate(function2, NULL);
    printf("Criou 2\n");
    cjoin(tid1);
    printf("Joined 1\n");
    cjoin(tid2);
    printf("Joined 2\n");
    return 0;
}
