#ifndef __scheduler__
#define __scheduler__

/*
* Initialization function for the library.
* It is responsible for initializing the main thread and the queues for managing
* the threads (enabled_queue, blocked_queue and joining_queue).
* It should be called only the first time the library is called.
* Return 0 in the case of success and -1 in the case of error.
*/
int _init_ ();

/*
* Calls the scheduler
*/
int swap_context();

/* Auxiliar function that deletes from the queue the element passed as parameter */
void remove_from_queue(PFILA2 queue, void* element);


#endif
